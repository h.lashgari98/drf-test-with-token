from django.db import models


class AbstractModel(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    modification_date = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Genre(AbstractModel):
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title

    class Meta:
        db_table = "mygenres"


class Author(AbstractModel):
    name = models.CharField(max_length=255)
    birth = models.IntegerField(null=True)
    death = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return "{} ({} - {})".format(self.name, self.birth, self.death or 'now')


class Book(AbstractModel):
    title = models.CharField(max_length=255)
    year = models.IntegerField(null=None)
    author = models.ForeignKey(Author, on_delete=models.PROTECT)
    genre = models.ManyToManyField(Genre)
    cover = models.ImageField(upload_to='book-covers/', null=True, blank=True)
    pdf = models.FileField(upload_to='pdf/', null=True, blank=True)

    def __str__(self):
        return "{} ({} - {})".format(self.title, self.year, self.author.name)