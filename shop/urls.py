from django.urls import path
from .views import *
from rest_framework.urlpatterns import format_suffix_patterns

app_name = "shop"

urlpatterns = [
    path('api/authors/', api_author_list),
    path('api/author/<int:pk>/', api_author_detail),
]

urlpatterns = format_suffix_patterns(urlpatterns)